# aAa.cs project

## Goal
Follow csgo matches, update scores and generate match summary.

```mainaaa.py``` follow a match, with Team1, Team2, Time (HH:mm) given in param
```analyze.py``` generate summary with given match data

```phrases.py``` set of words used to build coherent phrases


## Requirements

To build dependencies :

```
python setup.py aAacsgolive
```

min requirements :

```
beautifulsoup4==4.5.1

bs4==0.0.1

lxml==3.6.4

python_utils==2.0.0

requests==2.11.1

six=1.10.1 
