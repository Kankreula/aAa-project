#coding: utf-8
import sys
sys.path.insert(0, '../lib-hltv')
import matchlist
#TODO: utiliser service REST aAa


def updatescore(team1sc,team2sc):
    f = open('datadump','w')
    f.write("Resulat au round numero " + str(team1sc + team2sc) + ": " + str(team1sc) + " à " + str(team2sc))
    f.close()
def closematch(data,lr):
    f = open('datadump','w')
    f.write("Le match s'est terminé ici après " + lr + ". Le dump qui suit est le total des données collectées. \n" + data)
    f.close
def get_match_url(team1,team2,date):
    matchs = matchlist.getdaymatch()
    for match in matchs:
        #TODO: ajouter le modulo sur la date et les noms
        if match.findChild('div',{"class":"matchTimeCell"}).text == date and ((
            match.findChild('div',{"class" : "matchTeam1Cell"}).findChild('a',{}).text.replace("\n","").replace(" ","") == team1 and
            match.findChild('div',{"class" : "matchTeam2Cell"}).findChild('a',{}).text.replace("\n","").replace(" ","") == team2) or
            (match.findChild('div',{"class" : "matchTeam2Cell"}).findChild('a',{}).text.replace("\n","").replace(" ","") == team1 and
            match.findChild('div',{"class" : "matchTeam1Cell"}).findChild('a',{}).text.replace("\n","").replace(" ","") == team2)):
                return str(match.findChild('div',{"class" : "matchActionCell"}).findChild('a')["href"].replace("\n","").replace(" ",""))
    print("MATCH NOT FOUND")
