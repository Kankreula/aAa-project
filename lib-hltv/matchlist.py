import requests,re
import datetime
from bs4 import BeautifulSoup
from python_utils import converters

def get_parsed_page(url):
    return BeautifulSoup(requests.get(url).text, "lxml")

urls = "http://www.hltv.org/matches/"
matches = get_parsed_page(urls)
currdate = datetime.datetime.now()
def date_extention(number):
    number = int(number)
    if number < 20 and number > 10:
        return '%dth' % number
    if number%10 == 1:
        return '%dst' % number
    if number%10 == 2:
        return '%dnd' % number
    if number%10 == 3:
        return '%drd' % number
    if (number%10 >= 4) or (number%10== 0):
        return '%dth' % number
def formatedate():
    return currdate.strftime("%A, %B " + date_extention(currdate.strftime("%d")) + " %Y")
def getdaymatch():
    matcheslist = matches.findAll("div", {"class": "matchListDateBox"})
    for match in matcheslist:
        if match.text == formatedate():
            return match.findParent("div", {"class": "centerFade"}).findChildren("div", {"class": "matchListBox"})
    print("prout")
