#coding: utf8
import sys
sys.path.insert(0, 'lib-hltv')
sys.path.insert(0, 'lib-analyze')
import phrases
import random
import math

#Calculer le max de diff de round dans la game (entre 2 bornes, pour le premier half, pour le 2nd half)
def round_diff(data,r1=0,r2=0):
    maxdelta = 1
    at = 0
    print(len(data))
    if r2==0:
        r2=len(data)-1
    for i in range(r1,r2):
        if data[str(i)] is not None:
            if abs(data[str(i)]['score1'] - data[str(i)]['score2']) > maxdelta:
                maxdelta = data[str(i)]['score1'] - data[str(i)]['score2']
                at = i
    return [maxdelta, at]
def round_diff_1_mt(data):
    return round_diff(data,0,15)
def round_diff_2_mt(data):
    return round_diff(data,16,30)
#Genrateur de phrase
def select_phrase(phrase,param):
    currentphrase = ""
    #Test si 2 phrases de meme type sont en train d'être generer,
    #TODO:gerer le "err"
    if param[str(phrase[0][0])] == 1:
        return "err"
    #Construit les phrases
    for i in range(1,len(phrase)):
        currentphrase += phrase[i][random.randint(0,len(phrase[i]) -1)]
    #Met la valeur du type de phrase générer a 1 (ou ne touche pas si phrase quelconque)
    if phrase[0][0] != 0:
        param[str(phrase[0])] = 1;
    return currentphrase
def match_analyze():
    #data that will be given in parameter
    datatest = {'match' : 123, '0' : {'score1' : 0 ,'score2' : 0}, '1' :  {'score1' : 1,'score2' : 0},'2' :  {'score1' : 2,'score2' : 0},'3' :  {'score1' : 2,'score2' : 1}}
    score = ""
    joueur = ""
    #1 = intro, 2=transition, 3=conclusion #0=quelconque pour eviter redondance phrases
    param_phrases = {'0' : 0, '1' : 0, '2' : 0,'3' : 0}
    #TODO : imple conditions for phrases ex comeback, stomp etc
    all_phrases_list =  ['score','joueur','mitemps','comeback']
    all_joker_list = ['$score','$mvp',]
    phrases_list = []
    phrase_to_use = []
    #genere les phrases
    #TODO : pick random phrases (don't generate all phrases)
    nb_phrases = random.randint(math.ceil(len(all_phrases_list)/2),len(all_phrases_list))
    for i in range(0,nb_phrases):
        phrasenum = random.randint(0,len(all_phrases_list)-1)
        if all_phrases_list[phrasenum] not in phrase_to_use:
            phrase_to_use.append(all_phrases_list[i])
        else:
            --i
    for phrase in phrase_to_use:
        phrases_list.append(select_phrase(phrases.phrases[phrase][random.randint(0,len(phrases.phrases[phrase]) -1)],param_phrases))
    #affiche les phrases
    for p in phrases_list:
        print(p)
        #TODO : will need to use that in order to insert data phrases_list['score'].replace('$score', str(datatest['3']['score1']) + '-' + str(datatest['3']['score2']))
    diff = round_diff(datatest)
    print('La plus grosse diff était de ' +  str(diff[0]) + ' rounds d\'écart au ' + str(diff[1]) + ' eme round.')

match_analyze()
