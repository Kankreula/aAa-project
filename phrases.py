#coding: utf8
#1 = intro, 2=transition, 3=conclusion #0=quelconque
#vrai data
#phrases = { 'score' : [[[1],['Le score ', 'Le résultat '],['final ','du match ', 'de la rencontre '],['est de: ', 'est finalement: ','se trouve être: '],['$score']],[[2],['Lol ',' xd'],['mdr','mdrrr']]],'joueur': [[[1],['testtt'],['testouille']]]}
#lorem ipsum data
phrases = {
    "score": [
    [
      [1],
      [
        "fugiat ad",
        "esse cillum",
        "consequat est",
        "nulla sit",
        "adipisicing sunt"
      ],
      [
        "exercitation ullamco",
        "officia commodo",
        "cillum dolor",
        "exercitation minim",
        "do deserunt",
        "laborum officia"
      ],
      [
        "est ullamco",
        "culpa nostrud",
        "commodo labore",
        "duis fugiat",
        "ullamco officia",
        "aliquip aliquip",
        "quis voluptate",
        "ullamco incididunt"
      ],
      [
        "occaecat sint",
        "non dolore",
        "irure eiusmod",
        "ad dolore",
        "proident quis",
        "nulla cupidatat",
        "quis amet",
        "incididunt minim",
        "do ipsum"
      ]
    ],
    [
      [2],
      [
        "duis excepteur",
        "duis qui",
        "est ut",
        "non fugiat"
      ],
      [
        "aute anim",
        "tempor enim",
        "anim sunt",
        "fugiat laboris",
        "fugiat cillum",
        "proident nulla",
        "enim adipisicing"
      ],
      [
        "cupidatat nisi",
        "nulla sint",
        "velit non"
      ]
    ],
    [
      [0],
      [
        "deserunt non",
        "amet occaecat",
        "exercitation amet",
        "commodo anim",
        "voluptate aliqua"
      ],
      [
        "mollit anim",
        "ut ut",
        "tempor occaecat"
      ],
      [
        "sunt officia",
        "excepteur aliqua",
        "aliquip in",
        "nisi in",
        "consectetur occaecat",
        "est aliquip",
        "dolor amet",
        "id duis"
      ],
      [
        "in excepteur",
        "elit aliqua",
        "minim excepteur",
        "pariatur in",
        "occaecat laboris",
        "commodo do",
        "culpa ea",
        "eiusmod adipisicing",
        "amet eiusmod"
      ],
      [
        "consequat fugiat",
        "nostrud velit",
        "excepteur excepteur"
      ],
      [
        "aliqua aute",
        "qui veniam",
        "ullamco nulla",
        "mollit incididunt",
        "magna consectetur",
        "enim tempor",
        "ullamco esse"
      ],
      [
        "et dolore",
        "aliqua veniam",
        "culpa adipisicing",
        "occaecat voluptate",
        "adipisicing aliqua"
      ],
      [
        "veniam id",
        "irure dolore",
        "incididunt officia",
        "Lorem quis",
        "sunt velit",
        "laboris ea",
        "laborum deserunt",
        "reprehenderit excepteur",
        "laborum ad"
      ],
      [
        "veniam aute",
        "eiusmod ad",
        "do et",
        "ea ex"
      ]
    ],
    [
      [3],
      [
        "ad laboris",
        "commodo proident",
        "commodo ullamco",
        "quis sit",
        "veniam sit",
        "sit quis",
        "non velit",
        "do ea"
      ],
      [
        "voluptate ut",
        "labore tempor",
        "consectetur deserunt",
        "aliquip proident",
        "ipsum cupidatat"
      ],
      [
        "enim commodo",
        "ea do",
        "sit consequat"
      ],
      [
        "in mollit",
        "ex sunt",
        "est cupidatat",
        "pariatur ipsum",
        "culpa ullamco",
        "dolore enim"
      ],
      [
        "velit voluptate",
        "quis fugiat",
        "sunt elit"
      ],
      [
        "eiusmod sunt",
        "esse fugiat",
        "labore est",
        "velit deserunt"
      ]
    ],
    [
      [0],
      [
        "adipisicing excepteur",
        "eiusmod occaecat",
        "qui ullamco",
        "ea sint"
      ],
      [
        "exercitation anim",
        "sint eiusmod",
        "officia culpa",
        "dolore dolore",
        "culpa proident",
        "exercitation incididunt",
        "enim dolor",
        "ea minim"
      ],
      [
        "amet nostrud",
        "fugiat deserunt",
        "aute ullamco",
        "ut esse",
        "ea laborum",
        "ipsum proident",
        "exercitation velit",
        "consequat ea",
        "laboris Lorem"
      ],
      [
        "sint esse",
        "consectetur et",
        "amet cillum",
        "incididunt irure",
        "minim excepteur",
        "aliqua irure"
      ],
      [
        "sit elit",
        "consequat commodo",
        "ea non",
        "exercitation qui",
        "tempor reprehenderit",
        "cupidatat eiusmod",
        "est proident",
        "cillum ad"
      ],
      [
        "excepteur laboris",
        "reprehenderit voluptate",
        "officia est",
        "adipisicing nisi",
        "mollit nisi"
      ],
      [
        "sunt consectetur",
        "est cupidatat",
        "do elit",
        "amet in",
        "aliqua ex",
        "aliqua do"
      ],
      [
        "exercitation tempor",
        "exercitation deserunt",
        "consectetur enim",
        "sunt id",
        "exercitation et",
        "laboris incididunt"
      ],
      [
        "est ad",
        "occaecat eiusmod",
        "cillum dolor",
        "sunt adipisicing",
        "nulla nulla",
        "occaecat sit"
      ]
    ]
  ],
  "joueur": [
    [
      [1],
      [
        "in ullamco",
        "do reprehenderit",
        "voluptate voluptate",
        "aliquip excepteur",
        "minim nisi",
        "consequat veniam",
        "reprehenderit tempor",
        "eu labore",
        "proident ad"
      ],
      [
        "commodo reprehenderit",
        "in nostrud",
        "velit tempor",
        "incididunt ut"
      ],
      [
        "Lorem nulla",
        "dolor veniam",
        "aliqua incididunt",
        "quis proident",
        "esse sunt",
        "voluptate pariatur",
        "consectetur minim",
        "laboris pariatur"
      ],
      [
        "aute labore",
        "ea excepteur",
        "id ullamco"
      ],
      [
        "veniam laboris",
        "culpa excepteur",
        "veniam ex",
        "officia consequat",
        "pariatur enim",
        "minim labore"
      ]
    ],
    [
      [0],
      [
        "aute ex",
        "dolore est",
        "tempor mollit",
        "consequat pariatur",
        "occaecat est",
        "aliquip dolor",
        "laborum dolor"
      ],
      [
        "irure minim",
        "proident ipsum",
        "proident cupidatat",
        "magna ad",
        "irure occaecat",
        "irure ad",
        "mollit reprehenderit"
      ],
      [
        "non minim",
        "incididunt officia",
        "officia dolor",
        "proident voluptate"
      ],
      [
        "cupidatat amet",
        "culpa et",
        "occaecat reprehenderit",
        "quis quis"
      ],
      [
        "occaecat duis",
        "elit eu",
        "incididunt reprehenderit",
        "ex reprehenderit",
        "laborum nisi",
        "duis cupidatat",
        "exercitation ea"
      ],
      [
        "est sit",
        "excepteur dolore",
        "excepteur velit",
        "irure nulla",
        "nisi consectetur"
      ],
      [
        "est ex",
        "quis irure",
        "ullamco consequat",
        "elit culpa",
        "sunt aliqua",
        "aliqua pariatur",
        "fugiat in",
        "aliquip anim"
      ],
      [
        "labore nostrud",
        "ipsum Lorem",
        "deserunt et",
        "ullamco consequat",
        "minim ad"
      ]
    ],
    [
      [1],
      [
        "exercitation veniam",
        "elit elit",
        "sit proident",
        "eiusmod minim"
      ],
      [
        "commodo consectetur",
        "nulla officia",
        "exercitation laboris"
      ],
      [
        "esse pariatur",
        "culpa sit",
        "ullamco et"
      ],
      [
        "ut dolore",
        "ex elit",
        "cupidatat ullamco",
        "fugiat incididunt"
      ],
      [
        "mollit cupidatat",
        "aute anim",
        "veniam nostrud",
        "duis aliqua",
        "Lorem sunt",
        "laborum mollit",
        "consequat duis",
        "sunt irure"
      ],
      [
        "esse laborum",
        "velit aliquip",
        "mollit id"
      ],
      [
        "laboris elit",
        "incididunt aute",
        "pariatur in"
      ],
      [
        "nisi consequat",
        "velit nulla",
        "officia id",
        "excepteur elit",
        "commodo ea",
        "officia consequat",
        "fugiat excepteur",
        "velit culpa",
        "ea anim"
      ]
    ],
    [
      [0],
      [
        "laborum nostrud",
        "ipsum irure",
        "et duis",
        "ipsum excepteur",
        "cupidatat exercitation",
        "mollit cupidatat",
        "ad aute",
        "sunt eu",
        "veniam excepteur"
      ],
      [
        "eiusmod sint",
        "minim ex",
        "ullamco sunt",
        "minim laborum",
        "adipisicing eiusmod",
        "in nulla",
        "proident do",
        "dolor consectetur"
      ],
      [
        "adipisicing laboris",
        "eu quis",
        "velit aliquip",
        "adipisicing aliquip",
        "sit sunt",
        "ut officia"
      ],
      [
        "do incididunt",
        "dolore ex",
        "ea dolore",
        "id esse"
      ],
      [
        "incididunt adipisicing",
        "voluptate irure",
        "consectetur magna",
        "sunt quis",
        "ut esse",
        "id in",
        "Lorem consectetur"
      ],
      [
        "consectetur aute",
        "consectetur exercitation",
        "duis velit",
        "non incididunt",
        "esse mollit",
        "non tempor",
        "ex ea",
        "ad adipisicing"
      ]
    ],
    [
      [0],
      [
        "fugiat ipsum",
        "in qui",
        "voluptate non",
        "aliqua consectetur"
      ],
      [
        "ut do",
        "veniam aliqua",
        "culpa laboris",
        "et enim",
        "do do"
      ],
      [
        "in labore",
        "ex cillum",
        "veniam Lorem",
        "magna dolore",
        "sint fugiat",
        "quis cupidatat"
      ],
      [
        "sint nostrud",
        "reprehenderit culpa",
        "cillum qui"
      ]
    ],
    [
      [3],
      [
        "duis cillum",
        "quis consectetur",
        "eiusmod minim"
      ],
      [
        "ea consectetur",
        "reprehenderit tempor",
        "commodo eiusmod",
        "ipsum nostrud",
        "eiusmod ullamco"
      ],
      [
        "eu sit",
        "ea Lorem",
        "consectetur qui"
      ]
    ],
    [
      [1],
      [
        "qui in",
        "laboris aute",
        "esse occaecat",
        "do dolore"
      ],
      [
        "anim aute",
        "non exercitation",
        "voluptate amet"
      ],
      [
        "nostrud anim",
        "minim qui",
        "aute aliqua",
        "proident sunt",
        "pariatur ex",
        "non laborum",
        "ea excepteur"
      ],
      [
        "exercitation consectetur",
        "veniam deserunt",
        "deserunt laborum",
        "do adipisicing",
        "officia voluptate",
        "exercitation nulla",
        "enim occaecat",
        "enim enim",
        "voluptate proident"
      ]
    ]
  ],
  "mitemps": [
    [
      [1],
      [
        "esse duis",
        "ea non",
        "elit et",
        "eiusmod voluptate",
        "culpa deserunt",
        "ipsum occaecat",
        "fugiat laboris"
      ],
      [
        "nulla aute",
        "aliquip tempor",
        "nulla ad",
        "officia est",
        "minim ea",
        "incididunt reprehenderit",
        "aliqua deserunt",
        "anim amet"
      ],
      [
        "laboris exercitation",
        "non qui",
        "sit in",
        "mollit aliquip"
      ],
      [
        "laborum dolore",
        "labore nisi",
        "eu exercitation"
      ],
      [
        "qui quis",
        "ut magna",
        "velit sint",
        "proident laborum",
        "nisi aliquip",
        "sit laboris"
      ],
      [
        "laborum ullamco",
        "officia elit",
        "deserunt sunt",
        "ullamco magna",
        "reprehenderit in",
        "irure aute"
      ],
      [
        "exercitation esse",
        "incididunt est",
        "commodo irure"
      ],
      [
        "incididunt dolor",
        "ex occaecat",
        "incididunt aliquip",
        "quis commodo",
        "dolor ut",
        "veniam sunt",
        "quis sit",
        "mollit mollit"
      ],
      [
        "ipsum nisi",
        "veniam excepteur",
        "deserunt deserunt",
        "qui dolore",
        "velit magna"
      ]
    ],
    [
      [0],
      [
        "eu in",
        "aliquip nisi",
        "laboris id"
      ],
      [
        "esse labore",
        "minim amet",
        "irure eiusmod"
      ],
      [
        "magna ex",
        "ipsum elit",
        "ea sit",
        "aliquip eiusmod",
        "est consectetur",
        "cupidatat mollit",
        "duis dolor",
        "dolor mollit",
        "fugiat ad"
      ],
      [
        "ad officia",
        "reprehenderit ut",
        "exercitation enim",
        "nisi do",
        "ut ullamco",
        "sit id"
      ],
      [
        "esse aliquip",
        "incididunt non",
        "excepteur adipisicing",
        "ex culpa",
        "esse incididunt"
      ],
      [
        "elit velit",
        "et nostrud",
        "dolor nulla",
        "eu ullamco",
        "eiusmod duis"
      ],
      [
        "ad Lorem",
        "est enim",
        "enim non",
        "sunt aute"
      ],
      [
        "duis esse",
        "voluptate ex",
        "officia elit",
        "aliqua sit",
        "commodo voluptate",
        "duis qui",
        "veniam proident",
        "ea consequat",
        "excepteur mollit"
      ],
      [
        "amet ipsum",
        "do anim",
        "sit mollit",
        "aliquip officia",
        "dolor sunt",
        "officia dolore",
        "ea mollit"
      ]
    ],
    [
      [2],
      [
        "incididunt excepteur",
        "ad amet",
        "sunt irure",
        "enim aliquip",
        "culpa nostrud",
        "mollit fugiat",
        "irure qui",
        "voluptate laboris"
      ],
      [
        "ut veniam",
        "reprehenderit laborum",
        "laborum commodo",
        "minim deserunt",
        "proident fugiat",
        "consequat aute",
        "duis nostrud",
        "Lorem consequat"
      ],
      [
        "id anim",
        "tempor enim",
        "labore reprehenderit",
        "et ipsum"
      ],
      [
        "amet eu",
        "ullamco mollit",
        "exercitation nostrud",
        "sunt irure",
        "labore consequat",
        "occaecat pariatur"
      ],
      [
        "cupidatat ipsum",
        "proident qui",
        "proident Lorem",
        "officia esse",
        "incididunt esse",
        "ipsum aute",
        "voluptate ea",
        "irure qui",
        "nulla nostrud"
      ],
      [
        "duis veniam",
        "voluptate id",
        "exercitation commodo",
        "et culpa"
      ],
      [
        "consectetur ullamco",
        "amet occaecat",
        "esse commodo",
        "sit veniam",
        "eu sunt",
        "labore culpa",
        "adipisicing labore",
        "eiusmod sint"
      ],
      [
        "et elit",
        "ipsum amet",
        "esse irure"
      ]
    ],
    [
      [3],
      [
        "est nostrud",
        "nisi culpa",
        "laborum pariatur"
      ],
      [
        "adipisicing sit",
        "qui Lorem",
        "ullamco qui",
        "adipisicing labore",
        "non ea",
        "labore Lorem",
        "do labore",
        "duis proident"
      ],
      [
        "anim anim",
        "laboris quis",
        "proident dolore",
        "cupidatat velit",
        "minim laboris",
        "duis eu",
        "qui dolor"
      ],
      [
        "proident minim",
        "laborum ullamco",
        "ut minim",
        "occaecat sunt"
      ],
      [
        "laborum commodo",
        "ut dolore",
        "est proident",
        "aliqua sunt",
        "enim Lorem",
        "eu sint",
        "laborum ut",
        "eiusmod eiusmod",
        "nisi nisi"
      ],
      [
        "enim nostrud",
        "esse eu",
        "nostrud fugiat",
        "enim ullamco",
        "magna mollit",
        "aliqua sunt"
      ],
      [
        "mollit anim",
        "et tempor",
        "ex magna",
        "pariatur veniam",
        "ea deserunt"
      ]
    ],
    [
      [0],
      [
        "in sunt",
        "non laborum",
        "incididunt magna",
        "veniam et",
        "laborum duis",
        "proident ipsum",
        "dolor cupidatat",
        "adipisicing dolore",
        "ea est"
      ],
      [
        "id elit",
        "commodo occaecat",
        "cillum sit",
        "id deserunt",
        "non culpa"
      ],
      [
        "ullamco incididunt",
        "proident qui",
        "amet fugiat",
        "excepteur nulla",
        "non et",
        "nulla proident",
        "consequat adipisicing",
        "fugiat aute"
      ],
      [
        "sit occaecat",
        "in cillum",
        "consectetur eu",
        "aute laborum"
      ],
      [
        "enim commodo",
        "est ullamco",
        "duis cupidatat",
        "commodo cupidatat"
      ],
      [
        "incididunt tempor",
        "aliquip esse",
        "proident laboris",
        "proident do",
        "fugiat cillum",
        "elit nulla",
        "proident aliqua",
        "excepteur cupidatat",
        "pariatur nostrud"
      ]
    ],
    [
      [2],
      [
        "reprehenderit aliquip",
        "do laboris",
        "ut officia"
      ],
      [
        "consequat officia",
        "nulla nostrud",
        "dolore pariatur"
      ],
      [
        "commodo et",
        "irure incididunt",
        "sint incididunt"
      ],
      [
        "quis tempor",
        "reprehenderit est",
        "dolore cillum",
        "sint ipsum",
        "deserunt commodo"
      ],
      [
        "qui ipsum",
        "officia commodo",
        "aute qui",
        "fugiat culpa",
        "excepteur veniam",
        "qui dolore",
        "enim magna"
      ],
      [
        "consectetur aliquip",
        "nisi ex",
        "culpa dolore",
        "magna Lorem",
        "laborum aliquip",
        "dolore mollit",
        "ipsum in"
      ],
      [
        "do duis",
        "consectetur sint",
        "tempor occaecat",
        "ipsum labore",
        "occaecat tempor",
        "consectetur dolore",
        "consectetur eu",
        "fugiat reprehenderit"
      ]
    ],
    [
      [0],
      [
        "magna ad",
        "in officia",
        "ea labore",
        "tempor sit",
        "non occaecat",
        "culpa ut",
        "non consequat",
        "nulla consequat",
        "velit ea"
      ],
      [
        "excepteur dolor",
        "culpa magna",
        "magna exercitation"
      ],
      [
        "eiusmod nulla",
        "culpa dolor",
        "irure laborum"
      ]
    ]
  ],
  "comeback": [
    [
      [0],
      [
        "mollit aliquip",
        "fugiat est",
        "reprehenderit aute"
      ],
      [
        "qui tempor",
        "minim laborum",
        "laboris aliqua",
        "amet non",
        "reprehenderit exercitation"
      ],
      [
        "reprehenderit laboris",
        "laboris sint",
        "tempor cupidatat",
        "minim tempor",
        "magna sit"
      ],
      [
        "occaecat velit",
        "ad consequat",
        "esse ipsum",
        "enim adipisicing",
        "aliqua esse",
        "do cupidatat"
      ]
    ],
    [
      [0],
      [
        "exercitation sint",
        "ipsum sunt",
        "tempor non",
        "ad laborum",
        "quis duis",
        "adipisicing duis"
      ],
      [
        "officia cupidatat",
        "irure consequat",
        "quis quis",
        "sit incididunt",
        "Lorem laboris"
      ],
      [
        "culpa nostrud",
        "non veniam",
        "officia ipsum",
        "incididunt do",
        "voluptate aliqua"
      ],
      [
        "commodo et",
        "voluptate amet",
        "aute aliqua",
        "officia eiusmod"
      ],
      [
        "veniam irure",
        "exercitation eu",
        "deserunt sunt",
        "labore ipsum",
        "deserunt officia",
        "occaecat laboris",
        "qui est",
        "commodo mollit"
      ],
      [
        "ad et",
        "sint consectetur",
        "non ipsum"
      ],
      [
        "Lorem incididunt",
        "pariatur aliqua",
        "qui id",
        "minim ex",
        "in nostrud",
        "consectetur dolore",
        "voluptate est",
        "cillum sit"
      ],
      [
        "veniam ad",
        "adipisicing occaecat",
        "est consectetur",
        "et consequat",
        "enim officia"
      ],
      [
        "deserunt anim",
        "reprehenderit proident",
        "sint exercitation"
      ]
    ],
    [
      [0],
      [
        "occaecat ea",
        "aliquip occaecat",
        "Lorem pariatur",
        "velit occaecat",
        "ipsum dolore",
        "amet velit",
        "velit exercitation",
        "occaecat in",
        "eiusmod et"
      ],
      [
        "mollit quis",
        "magna esse",
        "ipsum aliqua",
        "id cillum",
        "ad sunt",
        "nisi reprehenderit",
        "non sint"
      ],
      [
        "sint nulla",
        "adipisicing deserunt",
        "aliqua veniam",
        "nulla enim",
        "aliqua ad",
        "id laboris",
        "proident proident",
        "eu id",
        "est culpa"
      ],
      [
        "eu aute",
        "laboris irure",
        "minim aliqua",
        "velit est",
        "deserunt sint",
        "nulla mollit"
      ],
      [
        "occaecat in",
        "pariatur velit",
        "in aliquip",
        "aute voluptate",
        "et quis",
        "do qui",
        "labore officia"
      ],
      [
        "proident do",
        "enim irure",
        "anim est",
        "officia enim",
        "do magna",
        "incididunt tempor",
        "et adipisicing",
        "et mollit"
      ]
    ],
    [
      [2],
      [
        "Lorem ea",
        "enim in",
        "qui nisi",
        "velit fugiat",
        "minim incididunt",
        "occaecat irure",
        "veniam sint",
        "nisi aliqua",
        "in mollit"
      ],
      [
        "esse reprehenderit",
        "voluptate dolore",
        "nostrud nulla"
      ],
      [
        "ex ea",
        "cillum quis",
        "mollit exercitation",
        "eu minim",
        "duis tempor"
      ],
      [
        "commodo dolore",
        "non ea",
        "laborum consectetur",
        "in officia",
        "occaecat sint",
        "elit do"
      ],
      [
        "officia laboris",
        "magna esse",
        "id adipisicing",
        "ad minim",
        "ipsum do",
        "consequat fugiat",
        "exercitation exercitation",
        "duis duis",
        "eu Lorem"
      ]
    ]
  ]
}
