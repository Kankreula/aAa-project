from setuptools import setup

setup(  name='aAacsgolive',
        version='0.1',
        url='https://framagit.org/Kankreula/aAa-project/',
        author='Stephen Abello',
        license='GNU',
        install_requires=[
            'b e a u t i f u l s o u p 4 ',
            'bs4',
            'lxml',
            'p y t h o n - u t i l s ',
            ' r e q u e s t s ',
            ' s i x '
        ],
        )
