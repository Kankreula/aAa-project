#!/usr/bin/python
import sys
sys.path.insert(0, 'lib-hltv')
sys.path.insert(0, 'lib-aAa')
import matches
import time
import aaa_tools

d = {}
def watch_match():
    lastround = 0
    while(matches.is_live() == 1):
        print("live")
        round_t = str(matches.get_team_1_score() + matches.get_team_1_score())
        print(matches.get_team_1_score()) #OK
        print(matches.get_team_2_score()) #OK
        if round_t in d:
            d[round_t] = {}
            d[round_t]['score1'] = matches.get_team_1_score()
            d[round_t]['score2'] = matches.get_team_2_score()
            #TODO : getshit shit done (MVP du round, diff de kills par rapport au round prec, etcc)
            aaa_tools.updatescore(d[round_t]['score1'],d[round_t]['score2'])
            ++lastround
        time.sleep(60) #time between each fetch
        start_time=time.time()
        matches.reparse()
        print('elapsed time for match parsing : ' + time.time() - start_time)
    if matches.is_live() == 2:
        aaa_tools.closematch(d,lastround)
        #put analyze() here when done
        print("Match fini,bonne nuit!")

#ex result {'match' : 123, '0' : {'score1' : 0,'score2' : 0}, '1' :  {'score1' : 1,'score2' : 0}}
def main(argv):
    if len(argv) is not 4:
        printf("3 arguments : Team1, Team2, Horaire du match (HH:mm)")
        return
    matches.set_url("http://www.hltv.org/" + str(aaa_tools.get_match_url(argv[1],argv[2],argv[3])))
    d['match'] = argv[1]
    if matches.is_live() == 1:
        watch_match()
        print (d)
    elif matches.is_live() == 0:
        print("notlive")
    else:
        print("already ended")
if __name__ == "__main__":
    main(sys.argv)
